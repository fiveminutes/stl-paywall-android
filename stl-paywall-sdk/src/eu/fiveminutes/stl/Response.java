package eu.fiveminutes.stl;

import org.json.JSONObject;

public abstract class Response {

	public abstract void fromJson(JSONObject json);
}
