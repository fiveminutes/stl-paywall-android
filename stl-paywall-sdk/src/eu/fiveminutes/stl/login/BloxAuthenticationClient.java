package eu.fiveminutes.stl.login;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Base64;

import com.loopj.android.http.JsonHttpResponseHandler;

public class BloxAuthenticationClient {

	public static interface PARAMS {
		public static final String USER = "user";
		public static final String PASSWORD = "password";
		public static final String ALLOW_OPEN_ID = "allow_openid";
	}

	private static final String URL = "https://%s/tncms/webservice/v1/user/authenticate/";
	private static String BLOX_AUTHORIZATION;
	
	public static void post(String urlBase, String user, String password, JsonHttpResponseHandler responseHandler) {
		AuthenticationAsyncTask task = new AuthenticationAsyncTask(responseHandler);
		task.execute(getUrl(urlBase), user, password);
	}

	public static void setAuthorizationParams(String key, String secret){
		BLOX_AUTHORIZATION = new String(Base64.encode((key + ":" + secret).getBytes(), Base64.NO_WRAP));
	}
	
	private static String getUrl(String urlBase) {
		String s = String.format(URL, urlBase);
		return s;
	}

	private static class AuthenticationAsyncTask extends AsyncTask<String, Void, JSONObject> {

		JsonHttpResponseHandler responseHandler = null;

		public AuthenticationAsyncTask(JsonHttpResponseHandler responseHandler) {
			this.responseHandler = responseHandler;
		}

		@Override
		protected void onPostExecute(JSONObject result) {
			responseHandler.onSuccess(200, result);
		}

		@Override
		protected JSONObject doInBackground(String... args) {
			String url = args[0];

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(PARAMS.USER, args[1]));
			params.add(new BasicNameValuePair(PARAMS.PASSWORD, args[2]));

			AndroidHttpClient client = AndroidHttpClient.newInstance("paywall");

			HttpPost post = new HttpPost(url);
			post.addHeader("Authorization",
					"Basic " + BLOX_AUTHORIZATION);
			HttpParams httpParams = new BasicHttpParams();
			HttpConnectionParams.setConnectionTimeout(httpParams, 15 * 1000);
			HttpConnectionParams.setSoTimeout(httpParams, 15 * 1000);
			httpParams.setParameter(ClientPNames.HANDLE_REDIRECTS, Boolean.TRUE);
			post.setParams(httpParams);

			HttpResponse response = null;
			StringBuilder responseBody = new StringBuilder();
			JSONObject res = null;
			try {
				post.setEntity(new UrlEncodedFormEntity(params));
				response = client.execute(post);
				BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
				String line;
				while ((line = reader.readLine()) != null) {
					responseBody.append(line);
				}
				res = new JSONObject(responseBody.toString());
			} catch (Exception e) {
				if (response != null) {
					responseHandler.onFailure(responseBody.toString(), e);
				} else {
					responseHandler.onFailure("", e);
				}
			} finally{
				if(client != null){
					client.close();
				}
			}
			return res;
		}
	}

}
