# STL Paywall Android SDK

This library manages access to paid content in your apps using the STL Paywall.


## Usage

- Add stl-paywall to your project as a library

- In your activity or framgent, instantiate paywall

	paywall = new Paywall(context, company, property, bloxUrl, bloxKey, bloxSecret);

	company, property - used to construct SYNCRONEX url
	bloxUrl - used to construct BLOX url
	bloxKey, bloxSecret - used for http authentication 

- Simplest way to use the sdk:
	- extend DefaultCheckAccessListener
	- override methods showContent(), hideContent(), onError(Throwable) and onUnauthorizedAfterLogin()
	- call checkAccess(new ExtendedDefaultCheckAccessListener(context)) 
	- see sample app for the usage example

- If you choose not to extend DefaultCheckAccessListener:
	- check if user is authorized: paywall.checkAccess(ResponseListener<CheckAccessResponse> callback)
	- login: paywall.login(String user, String password, ResponseListener<LoginResponse> callback)
	- clear stored user data: paywall.clearUser()


## 3rd party libraries

https://github.com/loopj/android-async-http